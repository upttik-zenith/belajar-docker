<?php

echo "My APP<br></br>";

$connection = new mysqli(getenv("MYSQL_HOSTNAME"), getenv("MYSQL_USERNAME"), getenv("MYSQL_PASSWORD"));

if ($connection->connect_errno) {
  die("Connection failed: " . $connection->connect_error);
}

echo "Connected successfully to MYSQL Database<br></br>";

$databases = $connection->query("show databases");

if ($databases){
    $databases = $databases->fetch_all(MYSQLI_ASSOC);
    echo "List Databases:";
    echo "<ul>";
    foreach ($databases as $key => $value){
        if (!in_array($value["Database"], ['information_schema', 'mysql', 'performance_schema', 'sys'])){
            echo "<li>".$value["Database"]."</li>";
        }
    }
    echo "</ul>";

    if (in_array(getenv("MYSQL_DATABASE"), array_column($databases, "Database"))){

        $connection->select_db(getenv("MYSQL_DATABASE"));
        $tables = $connection->query("show tables")->fetch_all(MYSQLI_ASSOC);

        echo "List Tables of ".getenv("MYSQL_DATABASE").":";
        echo "<ul>";
        foreach ($tables as $key => $value){
            echo "<li>".$value["Tables_in_".getenv("MYSQL_DATABASE")]."</li>";
        }
        echo "</ul>";

        echo "<style>table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
            }</style>";

        foreach ($tables as $key => $value){
            echo "Table of ".$value["Tables_in_".getenv("MYSQL_DATABASE")];
            $query = $connection->query("select * from ".$value["Tables_in_".getenv("MYSQL_DATABASE")]);

            echo "<table>
                    <tr>";
            foreach ($query->fetch_fields() as $key => $value){
                echo "<th>".$value->name."</th>";
            }
            echo "</tr>";
            foreach ($query->fetch_all(MYSQLI_ASSOC) as $key => $value){
                echo "<tr>";
                foreach ($query->fetch_fields() as $key2 => $value2){
                    echo "<td>".$value[$value2->name]."</td>";
                }
                echo "</tr>";
            }
            echo "</table><br>";
        }

    }
}

?>